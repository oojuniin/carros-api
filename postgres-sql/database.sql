
create table if not exists carro (
   id  bigserial not null,
    descricao varchar(255),
    latitude varchar(255),
    longitude varchar(255),
    nome varchar(255),
    tipo varchar(255),
    url_foto varchar(255),
    url_video varchar(255),
    primary key (id)
);

create table if not exists usuario (
   id  bigserial not null,
    email varchar(255),
    login varchar(255),
    nome varchar(255),
    senha varchar(255),
    primary key (id)
);

create table if not exists user_roles (
   user_id bigint not null,
   role_id bigint not null
);

alter table user_roles
   add constraint fk_user_roles_role
   foreign key (role_id)
   references role (id);

alter table user_roles
   add constraint fk_user_roles_user
   foreign key (user_id)
   references user (id);
  
insert into role(nome) values ('role_user');
insert into role(nome) values ('role_admin');

insert into usuario (email, login, nome, senha) values ('usuario@gmail.com', 'usuario', 'usuario', '$2a$10$JQmHl0OZpo/yrBhMKmD1W.nCzMxzyRcMLtlvpJNHGZdI5Swvwlhlu');
insert into usuario (email, login, nome, senha) values ('admin@gmail.com', 'admin', 'admin', '$2a$10$JQmHl0OZpo/yrBhMKmD1W.nCzMxzyRcMLtlvpJNHGZdI5Swvwlhlu');
insert into usuario (email, login, nome, senha) values ('junior@gmail.com', 'junior', 'junior', '$2a$10$JQmHl0OZpo/yrBhMKmD1W.nCzMxzyRcMLtlvpJNHGZdI5Swvwlhlu');

insert into user_roles(user_id,role_id) values(1, 1);
insert into user_roles(user_id,role_id) values(2, 2);
insert into user_roles(user_id,role_id) values(3, 1);

insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('tucker 1948','descrição tucker 1948','http://www.livroandroid.com.br/livro/carros/classicos/tucker.png','http://www.livroandroid.com.br/livro/carros/classicos/tucker.mp4','-23.564224','-46.653156','classicos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('chevrolet corvette','descrição chevrolet corvette','http://www.livroandroid.com.br/livro/carros/classicos/chevrolet_corvette.png','http://www.livroandroid.com.br/livro/carros/classicos/corvette.mp4','-23.564224','-46.653156','classicos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('chevrolet impala coupe','descrição chevrolet impala coupe','http://www.livroandroid.com.br/livro/carros/classicos/chevrolet_impala_coupe.png','http://www.livroandroid.com.br/livro/carros/classicos/chevrolet_impala.mp4','-23.564224','-46.653156','classicos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('cadillac deville convertible','descrição cadillac deville convertible','http://www.livroandroid.com.br/livro/carros/classicos/cadillac_deville_convertible.png','http://www.livroandroid.com.br/livro/carros/classicos/cadillac_deville.mp4','-23.564224','-46.653156','classicos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('chevrolet bel-air','descrição chevrolet bel-air','http://www.livroandroid.com.br/livro/carros/classicos/chevrolet_belair.png','http://www.livroandroid.com.br/livro/carros/classicos/chevrolet_bel_air.mp4','-23.564224','-46.653156','classicos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('cadillac eldorado','descrição cadillac eldorado','http://www.livroandroid.com.br/livro/carros/classicos/cadillac_eldorado.png','http://www.livroandroid.com.br/livro/carros/classicos/cadillac_eldorado.mp4','-23.564224','-46.653156','classicos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('ferrari 250 gto','descrição ferrari 250 gto','http://www.livroandroid.com.br/livro/carros/classicos/ferrari_250_gto.png','http://www.livroandroid.com.br/livro/carros/classicos/ferrari_gto.mp4','-23.564224','-46.653156','classicos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('dodge challenger','descrição dodge challenger','http://www.livroandroid.com.br/livro/carros/classicos/dodge_challenger.png','http://www.livroandroid.com.br/livro/carros/classicos/dodge_challenger.mp4','','','classicos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('camaro ss 1969','descrição camaro ss 1969','http://www.livroandroid.com.br/livro/carros/classicos/camaro_ss.png','http://www.livroandroid.com.br/livro/carros/classicos/camaro_ss.mp4','-23.564224','-46.653156','classicos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('ford mustang 1976','descrição ford mustang 1976','http://www.livroandroid.com.br/livro/carros/classicos/ford_mustang.png','http://www.livroandroid.com.br/livro/carros/classicos/ford_mustang.mp4','-23.564224','-46.653156','classicos');

insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('ferrari ff','descrição ferrari ff','http://www.livroandroid.com.br/livro/carros/esportivos/ferrari_ff.png','http://www.livroandroid.com.br/livro/carros/esportivos/ferrari_ff.mp4','44.532218','10.864019','esportivos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('audi gt spyder','descrição audi gt spyder','http://www.livroandroid.com.br/livro/carros/esportivos/audi_spyder.png','http://www.livroandroid.com.br/livro/carros/esportivos/audi_gt.mp4','-23.564224','-46.653156','esportivos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('porsche panamera','descrição porsche panamera','http://www.livroandroid.com.br/livro/carros/esportivos/porsche_panamera.png','http://www.livroandroid.com.br/livro/carros/esportivos/porsche_panamera.mp4','-23.564224','-46.653156','esportivos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('lamborghini aventador','descrição lamborghini aventador','http://www.livroandroid.com.br/livro/carros/esportivos/lamborghini_aventador.png','http://www.livroandroid.com.br/livro/carros/esportivos/lamborghini_aventador.mp4','-23.564224','-46.653156','esportivos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('chevrolet corvette z06','descrição chevrolet corvette z06','http://www.livroandroid.com.br/livro/carros/esportivos/chevrolet_corvette_z06.png','http://www.livroandroid.com.br/livro/carros/esportivos/chevrolet_corvette.mp4','-23.564224','-46.653156','esportivos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('bmw m5','descrição bmw m5','http://www.livroandroid.com.br/livro/carros/esportivos/bmw.png','http://www.livroandroid.com.br/livro/carros/esportivos/bmw-m5.mp4','-23.564224','-46.653156','esportivos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('renault megane rs trophy','descrição renault megane rs trophy','http://www.livroandroid.com.br/livro/carros/esportivos/renault_megane_trophy.png','http://www.livroandroid.com.br/livro/carros/esportivos/renault_megane.mp4','-23.564224','-46.653156','esportivos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('maserati grancabrio sport','descrição maserati grancabrio sport','http://www.livroandroid.com.br/livro/carros/esportivos/maserati_grancabrio_sport.png','http://www.livroandroid.com.br/livro/carros/esportivos/renault_megane.mp4','-23.564224','-46.653156','esportivos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('mclaren mp4-12c','descrição mclaren mp4-12c','http://www.livroandroid.com.br/livro/carros/esportivos/mclaren.png','http://www.livroandroid.com.br/livro/carros/esportivos/mclaren.mp4','-23.564224','-46.653156','esportivos');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('mercedes-benz c63 amg','descrição mercedes-benz c63 amg','http://www.livroandroid.com.br/livro/carros/esportivos/mercedes_benz_amg.png','http://www.livroandroid.com.br/livro/carros/esportivos/mercedes.mp4','-23.564224','-46.653156','esportivos');

insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('bugatti veyron','descrição bugatti veyron','http://www.livroandroid.com.br/livro/carros/luxo/bugatti_veyron.png','http://www.livroandroid.com.br/livro/carros/luxo/bugatti_veyron.mp4','-23.564224','-46.653156','luxo');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('ferrari enzo','descrição ferrari enzo','http://www.livroandroid.com.br/livro/carros/luxo/ferrari_enzo.png','http://www.livroandroid.com.br/livro/carros/luxo/ferrari_enzo.mp4','-23.564224','-46.653156','luxo');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('lamborghini reventon','descrição lamborghini reventon','http://www.livroandroid.com.br/livro/carros/luxo/lamborghini_reventon.png','http://www.livroandroid.com.br/livro/carros/luxo/lamborghini _reventon.mp4','-23.564224','-46.653156','luxo');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('leblanc mirabeau','descrição leblanc mirabeau','http://www.livroandroid.com.br/livro/carros/luxo/leblanc_mirabeau.png','http://www.livroandroid.com.br/livro/carros/luxo/leblanc_mirabeau.mp4','-23.564224','-46.653156','luxo');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('shelby supercars ultimate','descrição shelby supercars ultimate','http://www.livroandroid.com.br/livro/carros/luxo/shelby_supercars_ultimate.png','http://www.livroandroid.com.br/livro/carros/luxo/shelby.mp4','-23.564224','-46.653156','luxo');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('pagani zonda','descrição pagani zonda','http://www.livroandroid.com.br/livro/carros/luxo/pagani_zonda.png','http://www.livroandroid.com.br/livro/carros/luxo/pagani_zonda.mp4','-23.564224','-46.653156','luxo');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('koenigsegg ccx','descrição koenigsegg ccx','http://www.livroandroid.com.br/livro/carros/luxo/koenigsegg_ccx.png','http://www.livroandroid.com.br/livro/carros/luxo/koenigsegg.mp4','-23.564224','-46.653156','luxo');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('mercedes slr mclaren','descrição mercedes slr mclaren','http://www.livroandroid.com.br/livro/carros/luxo/mercedes_mclaren.png','http://www.livroandroid.com.br/livro/carros/luxo/mclaren_slr.mp4','-23.564224','-46.653156','luxo');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('rolls royce phantom','descrição rolls royce phantom','http://www.livroandroid.com.br/livro/carros/luxo/rolls_royce_phantom.png','http://www.livroandroid.com.br/livro/carros/luxo/rolls_royce.mp4','-23.564224','-46.653156','luxo');
insert into carro (nome,descricao,url_foto,url_video,latitude,longitude,tipo) values('lexus lfa','descrição lexus lfa','http://www.livroandroid.com.br/livro/carros/luxo/lexus_lfa.png','http://www.livroandroid.com.br/livro/carros/luxo/lexus.mp4','-23.564224','-46.653156','luxo');
