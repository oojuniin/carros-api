
    create table Carro (
       id  bigserial not null,
        descricao varchar(255),
        latitude varchar(255),
        longitude varchar(255),
        nome varchar(255),
        tipo varchar(255),
        url_foto varchar(255),
        url_video varchar(255),
        primary key (id)
    );

    create table roles (
       id  bigserial not null,
        nome varchar(255),
        primary key (id)
    );

    create table user_roles (
       user_id int8 not null,
        role_id int8 not null
    );

    create table Usuario (
       id  bigserial not null,
        email varchar(255),
        login varchar(255),
        nome varchar(255),
        senha varchar(255),
        primary key (id)
    );

    alter table user_roles 
       add constraint FKh8ciramu9cc9q3qcqiv4ue8a6 
       foreign key (role_id) 
       references roles;

    alter table user_roles 
       add constraint FK64govmhaeaqu6o910s72staoe 
       foreign key (user_id) 
       references Usuario;
