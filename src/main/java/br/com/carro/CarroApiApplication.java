package br.com.carro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarroApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(CarroApiApplication.class, args);
	}
}
