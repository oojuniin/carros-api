package br.com.carro.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.carro.api.domain.Usuario;
import br.com.carro.api.repository.UsuarioRepository;

@Service(value = "userDetailService")
public class UserDetailServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioRepository usuarioRepositorio;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepositorio.findByLogin(username);

		if (usuario == null) {
			throw new UsernameNotFoundException("User not found");
		}

		return usuario;
	}
}
