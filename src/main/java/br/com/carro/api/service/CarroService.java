package br.com.carro.api.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import br.com.carro.api.domain.Carro;
import br.com.carro.api.dto.CarroDTO;
import br.com.carro.api.repository.CarroRepository;
import br.com.carro.exception.ObjectNotFoundException;

@Service
public class CarroService implements Serializable {
	private static final long serialVersionUID = 1L;

	@Autowired
	@Transient
	private CarroRepository repository;

	@Transactional(readOnly = true)
	public List<CarroDTO> findAll() {
		return repository.findAll().stream().map(CarroDTO::createCarroDTO).collect(Collectors.toList());

		// List<Carro> carros = repository.findAll();
		// return carros.stream().map(CarroDTO::new).collect(Collectors.toList());

		// return carros.stream().map(c-> CarroDTO(c)).collect(Collectors.toList());

		// List<CarroDTO> carrosDTO = new ArrayList<>();
		// for(Carro c : carros) {
		// carrosDTO.add(new CarroDTO(c));
		// }
		// return carrosDTO;
	}

	@Transactional(readOnly = true)
	public CarroDTO findById(Long id) {
		Optional<Carro> carro = repository.findById(id);
		return carro.map(CarroDTO::createCarroDTO).orElseThrow(() -> new ObjectNotFoundException("Carro não encontrado."));

		// Optional<Carro> carro = repository.findById(id);
		// return carro.map(c -> Optional.of(new CarroDTO(c))).orElse(null);

		// if (carro.isPresent()) {
		// return Optional.of(new CarroDTO(carro.get()));
		// } else {
		// return null;
		// }
	}

	@Transactional(readOnly = true)
	public List<CarroDTO> findByTipo(String tipo) {
		return repository.findByTipo(tipo).stream().map(CarroDTO::createCarroDTO).collect(Collectors.toList());
	}

	@Transactional
	public CarroDTO save(Carro carro) {
		Assert.isNull(carro.getNome(), "Não foi possível atualizar o registro.");
		return CarroDTO.createCarroDTO(repository.save(carro));
	}

	@Transactional
	public CarroDTO update(Carro carro, Long id) {
		Assert.notNull(id, "Não foi possível atualizar o registro.");

		Optional<Carro> carroBD = repository.findById(id);
		if (carroBD.isPresent()) {
			Carro update = carroBD.get();
			update.setNome(carro.getNome());
			update.setTipo(carro.getTipo());
			return CarroDTO.createCarroDTO(repository.save(update));
		}
		throw new RuntimeException("Não fo possível atualizar o registro.");

		// findById(id).map(db -> {
		// db.setNome(carro.getNome());
		// db.setTipo(carro.getTipo());
		// return repository.save(db);
		// }).orElseThrow(
		// () -> new RuntimeException("Não foi possível atualizar o registro."));
	}

	@Transactional
	public void delete(Long id) {
		repository.deleteById(id);
	}
}
