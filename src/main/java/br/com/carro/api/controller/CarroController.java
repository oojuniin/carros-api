package br.com.carro.api.controller;

import java.io.Serializable;
import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.carro.api.domain.Carro;
import br.com.carro.api.dto.CarroDTO;
import br.com.carro.api.service.CarroService;

@RestController
@RequestMapping("/api/v1/carros")
public class CarroController implements Serializable {
	private static final long serialVersionUID = 1L;

	@Autowired
	private CarroService service;

	@GetMapping
	public ResponseEntity<List<CarroDTO>> findAll() {
		List<CarroDTO> carrosDTO = service.findAll();
		return carrosDTO.isEmpty() ? ResponseEntity.notFound().build() : ResponseEntity.ok(carrosDTO);

//		return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<CarroDTO> findById(@PathVariable("id") Long id) {
		return ResponseEntity.ok(service.findById(id));

//		return carro.isEmpty() ?
//				ResponseEntity.notFound().build():
//				ResponseEntity.ok(carro.get());

//		if (carro.isEmpty()) {
//			return ResponseEntity.notFound().build();
//		}

//		return ResponseEntity.ok(carro.get());

//		return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping("/tipo/{tipo}")
	public ResponseEntity<List<CarroDTO>> findByTipo(@PathVariable("tipo") String tipo) {
		List<CarroDTO> carrosDTO = service.findByTipo(tipo);
		return carrosDTO.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(carrosDTO);

//		return new ResponseEntity<>(service.findByTipo(tipo), HttpStatus.OK);
	}

	@GetMapping("/tipo")
	public ResponseEntity<List<CarroDTO>> findByTipo2(@RequestParam String tipo) {
		return findByTipo(tipo);
	}

	@PostMapping
//	@Secured("role_admin")
	public ResponseEntity<CarroDTO> save(@RequestBody Carro carro) {
		CarroDTO carroDTO = service.save(carro);
		URI location = getUri(carroDTO.getId());
		return ResponseEntity.created(location).build();
	}
	
	private URI getUri(Long id) {
        return ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(id).toUri();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<CarroDTO> update(@RequestBody Carro carro, @PathVariable("id") Long id) {
		try {
			return ResponseEntity.ok(service.update(carro, id));
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
//		return CarroDTO.createCarroDTO(carro) != null
//				? ResponseEntity.ok(service.update(carro, id))
//				: ResponseEntity.notFound().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<CarroDTO> delete(@PathVariable("id") Long id) {
		service.delete(id);
		return ResponseEntity.ok().build();
	}
}
